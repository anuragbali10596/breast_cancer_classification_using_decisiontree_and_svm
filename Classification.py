from sklearn.datasets import load_breast_cancer
from sklearn.tree import DecisionTreeClassifier        
from sklearn.svm import SVC                            
from sklearn.model_selection import train_test_split
import matplotlib.pylab as plt
import numpy as np

cancer=load_breast_cancer()

print('Data => \n',cancer.data.shape)
feature_names=cancer.feature_names
target_names=cancer.target_names
target=cancer.target
features=cancer.data

X_train,X_test,y_train,y_test=train_test_split(features,target,stratify=target,random_state=66,test_size=0.25)
min_train = X_train.min(axis=0)
range_train=(X_train-min_train).max(axis=0)
X_train= (X_train - min_train)/range_train
X_test= (X_test - min_train)/range_train
print('Scaling Min feature\n',X_train.min(axis=0))
print('Scaling Max feature\n',X_train.max(axis=0))

print('Shape of Training Data => \n ',X_train.shape)
print('Shape of Testing Data => \n ',X_test.shape)

from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.metrics import mean_squared_error 
classifier = SVC(kernel = 'linear', random_state = 0)#kernal = 'linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’
trained_model=classifier.fit(X_train,y_train)
y_pred = classifier.predict(X_test)
print(len(y_pred))
cm_SVM = confusion_matrix(y_test, y_pred)
cm = np.array(confusion_matrix(y_test, y_pred,labels=[1,0]))
confussion_df=pd.DataFrame(cm,index=['is_cancer','is_healthy'],columns=['predicted_cancer','predicted_healthy'])
print("Accuracy score SVM during testing")
print(accuracy_score(y_test, y_pred)*100)
RMSE=np.square(np.subtract(y_test,y_pred)).mean()
print("Root Mean Squared Error \n",RMSE)

import seaborn as sns
sns.heatmap(confussion_df,annot=True)
print(confussion_df)
plt.show()

###### For Decision Tree without feature Scaling ######

cancer=load_breast_cancer()
x=cancer.data
y=cancer.target
print('Features',x.shape)
print('Target',y.shape)

X_train,X_test,y_train,y_test=train_test_split(x,y,stratify=y,random_state=72,test_size=.20)
print('X_train => \n',X_train.shape)
print('X_test => \n',X_test.shape)
print('y_train => \n',y_train.shape)
print('y_test => \n',y_test.shape)

from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='gini', splitter='best', max_depth=12, min_samples_split=2, min_samples_leaf=1,random_state=0)

classifier.fit(X_train,y_train)
y_pred=classifier.predict(X_test)

cm_dt = confusion_matrix(y_test, y_pred)
cm = np.array(confusion_matrix(y_test, y_pred,labels=[1,0]))
confussion_df=pd.DataFrame(cm,index=['is_cancer','is_healthy'],columns=['predicted_cancer','predicted_healthy'])

print("Accuracy score Decision Tree during testing")
print(accuracy_score(y_test, y_pred)*100)
print(len(y_pred))
RMSE=np.square(np.subtract(y_test,y_pred)).mean()
print("Root Mean Squared Error \n",RMSE)

import seaborn as sns
sns.heatmap(confussion_df,annot=True)
print(confussion_df)
plt.show()

### Decision Tree with feature Scaling ###

cancer=load_breast_cancer()
x=cancer.data
y=cancer.target
print('Features',x.shape)
print('Target',y.shape)

X_train,X_test,y_train,y_test=train_test_split(x,y,stratify=y,random_state=72,test_size=.20)
print('X_train => \n',X_train.shape)
print('X_test => \n',X_test.shape)
print('y_train => \n',y_train.shape)
print('y_test => \n',y_test.shape)

from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='entropy', splitter='best', max_depth=12, min_samples_split=2,random_state=0)

classifier.fit(X_train,y_train)
y_pred=classifier.predict(X_test)

cm_dt = confusion_matrix(y_test, y_pred)
cm = np.array(confusion_matrix(y_test, y_pred,labels=[1,0]))
confussion_df=pd.DataFrame(cm,index=['is_cancer','is_healthy'],columns=['predicted_cancer','predicted_healthy'])

print("Accuracy score Decision Tree during testing")
print(accuracy_score(y_test, y_pred)*100)
print("Total number of predictions => \n",len(y_pred))
RMSE=np.square(np.subtract(y_test,y_pred)).mean()
print("Root Mean Squared Error \n",RMSE)

import seaborn as sns
sns.heatmap(confussion_df,annot=True)
print(confussion_df)
plt.show()